'use strict';
var React = require('react-native');

var Router = require('gb-native-router');

//var Router = require('react-native-simple-router');

var login = require('./pages/login');

var {
  AppRegistry,
  StyleSheet,
  MySceneComponent,
} = React;

const firstRoute = {
  name: "Login",
  component: login,
};

var WSUAthleticsApp = React.createClass({
  render: function(){
  return (
    
      <Router 
      firstRoute={firstRoute}
      headerStyle={styles.navbarColor}
      />
    )
  }
});

const styles = StyleSheet.create({
  navbarColor: {
    backgroundColor: 'grey'
  }
});

module.exports = WSUAthleticsApp;

AppRegistry.registerComponent('WSUAthleticsApp', () => WSUAthleticsApp);
