'use strict';

var React = require('react-native');

var {
	View,
	Text,
	AppRegistry,
	Component,
	TextInput,
	StyleSheet,
	TouchableHighlight,
	Picker,
} = React;

// const DropDown = require('react-native-dropdown');

// const {
// 	Select,
// 	Option,
// 	OptionList,
// 	updatePosition
// } = DropDown;

var amsurvey = React.createClass ({

	getInitialState: function() {
		return {
			heartRate: 0,
			sleep: '',
			fatigue: '',
			cheerfulness: '',
			frustration: '',
			soreness: '',
		};
	},

	// componentDidMount: function() {
	// 	updatePosition(this.refs['SELECT1']);
	// 	updatePosition(this.refs['SELECT2']);
	// 	updatePosition(this.refs['SELECT3']);
	// 	updatePosition(this.refs['SELECT4']);
	// 	updatePosition(this.refs['SELECT5']);

	// 	updatePosition(this.refs['OPTIONLIST']);
	// },

	getOptionList: function() {
		return this.refs['OPTIONLIST'];
	},

	render(){
		return(
		<View style={styles.container}>
			<Text style={styles.whiteFont}>Morning HeartRate</Text>
			<View style={styles.textEdit}>
				<TextInput
				height={34}
				width={199}
				onChangeText={(heartRate) => this.setState({heartRate})}
				value={this.state.text}
				/>
			</View>

			<View style={{ height: 2 }}/>
 			
 			<Text style={styles.whiteFont}>Select quality of sleep: </Text>
 			<View style={styles.textEdit}>
			<Picker 
  				selectedValue={this.state.sleep}
  				onValueChange={(sleep) => this.setState({sleep})}>
  				<Picker.Item label="0" value="0" />
  				<Picker.Item label="1" value="1" />
  				<Picker.Item label="2" value="2" />
  				<Picker.Item label="3" value="3" />
  				<Picker.Item label="4" value="4" />
  				<Picker.Item label="5" value="5" />
			</Picker>
			</View>

	        <View style={{ height: 2 }}/>

	        <Text style={styles.whiteFont}>Select level of fatigue: </Text>
	        <View style={styles.textEdit}>
	        <Picker 
  				selectedValue={this.state.fatigue}
  				onValueChange={(fatigue) => this.setState({fatigue})}>
  				<Picker.Item label="0" value="0" />
  				<Picker.Item label="1" value="1" />
  				<Picker.Item label="2" value="2" />
  				<Picker.Item label="3" value="3" />
  				<Picker.Item label="4" value="4" />
  				<Picker.Item label="5" value="5" />
			</Picker>
			</View>
	         

	          <View style={{ height: 2 }}></View>

	          <Text style={styles.whiteFont}>Select value for cheerfulness: </Text>
	          <View style={styles.textEdit}>
	          <Picker 
  				selectedValue={this.state.cheerfulness}
  				onValueChange={(cheerfulness) => this.setState({cheerfulness})}>
  				<Picker.Item label="0" value="0" />
  				<Picker.Item label="1" value="1" />
  				<Picker.Item label="2" value="2" />
  				<Picker.Item label="3" value="3" />
  				<Picker.Item label="4" value="4" />
  				<Picker.Item label="5" value="5" />
			</Picker>
			</View>
	       

	        <View style={{ height: 2 }}/>

	        <Text style={styles.whiteFont}>Select level of frustation: </Text>
	        <View style={styles.textEdit}>
	        <Picker 
  				selectedValue={this.state.frustation}
  				onValueChange={(frustation) => this.setState({frustation})}>
  				<Picker.Item label="0" value="0" />
  				<Picker.Item label="1" value="1" />
  				<Picker.Item label="2" value="2" />
  				<Picker.Item label="3" value="3" />
  				<Picker.Item label="4" value="4" />
  				<Picker.Item label="5" value="5" />
			</Picker>
			</View>
	        

	        <View style={{ height: 2 }}/>

	        <Text style={styles.whiteFont}>Select value for soreness: </Text>
	        <View style={styles.textEdit}>
	       	<Picker 
  				selectedValue={this.state.soreness}
  				onValueChange={(soreness) => this.setState({soreness})}>
  				<Picker.Item label="0" value="0" />
  				<Picker.Item label="1" value="1" />
  				<Picker.Item label="2" value="2" />
  				<Picker.Item label="3" value="3" />
  				<Picker.Item label="4" value="4" />
  				<Picker.Item label="5" value="5" />
			</Picker>
			</View>

	        <View style={{ height: 3 }}/>

	        <View style={styles.button}>
          		<TouchableHighlight onPress={this.PLACEHOLDER}> 
            		<Text style={styles.whiteFont}>Submit</Text>
          		</TouchableHighlight>
        	</View>
	
		</View>
		);
	}
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'grey',
    padding: 20, 
  },
  textEdit: {
    height: 40,
    borderColor: 'grey',
    backgroundColor: 'white',
    borderWidth: 1,
    width: 200
  },
  button: {
    backgroundColor: 'green',
    padding: 10,
    marginBottom: 50,
    width: 65
  },
  whiteFont: {
    color: '#FFF'
  },
});

module.exports = amsurvey;