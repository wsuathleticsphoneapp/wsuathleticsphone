'use strict';

var React = require('react-native');
var main = require('./main');
var login = require('./login');
var {
  Image,
  ListView,
  TouchableHighlight,
  StyleSheet,
  RecyclerViewBackedScrollView,
  Text,
  View,
  TextInput,
} = React;

var editaccount = React.createClass({
getInitialState: function() {
  return {firstName: "", lastName: "", team: "", wNumber: "", password1: "", password2: "", data:[]}
},
mainPage: function() { // also fails not sure why something about the stack created by navigator
    this.props.replaceRoute({
      name: "Main Menu",
      component: main
    });
},

  render(){
  return (
    <View style={styles.container}>
      <View>

      <Text style={styles.whiteFont}>First Name:</Text>
        <TextInput style={styles.textEdit}
              placeholder="firstName"
              value={this.setState.firstName}
              onChangeText={(firstName) => this.setState({firstName})}
              />

      <Text style={styles.whiteFont}>Last Name:</Text>
        <TextInput style={styles.textEdit}
              placeholder="lastName"
              value={this.setState.lastName}
              onChangeText={(lastName) => this.setState({lastName})}
              />

      <Text style={styles.whiteFont}>W#:</Text>
        <TextInput style={styles.textEdit}
              placeholder="wNnumber"
              value={this.setState.wNumber}
              onChangeText={(wNumber) => this.setState({wNumber})}
              />  

      <Text style={styles.whiteFont}>New Password:</Text>
        <TextInput style={styles.textEdit}
              placeholder="New password"
              value={this.setState.password1}
              onChangeText={(password1) => this.setState({password1})}
              />  

      <Text style={styles.whiteFont}>Confirm New Password:</Text>
        <TextInput style={styles.textEdit}
              placeholder="Comfirm password"
              value={this.setState.password2}
              onChangeText={(password2) => this.setState({password2})}
              />  

      <View style={{ height: 3 }}/>

      <View style={styles.button}>
          <TouchableHighlight onPress={this.mainPage}>
            <Text style={styles.whiteFont}>Sign In</Text>
          </TouchableHighlight>
        </View>
      </View>
    </View>
    )
  }
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: 'grey'
  },
  textEdit: {
    height: 40,
    borderColor: 'grey',
    backgroundColor: 'white',
    borderWidth: 1,
    width: 250
  },
  whiteFont: {
    color: '#FFF'
  },
  button: {
    backgroundColor: 'green',
    padding: 10,
    marginBottom: 50,
    width: 65
  },

});

module.exports = editaccount;