'use strict';

var React = require('react-native');

var RestKit = require('react-native-rest-kit');

var main = require('./main');

var {
  AppRegistry,
  AsyncStorage,
  Component,
  StyleSheet,
  Text,
  TextInput,
  windowSize,
  Image,
  View,
  ToastAndroid,
  TouchableHighlight
} = React;

//var UIExplorerBlock = require('UIExplorerBlock');
//var UIExplorerPage = require('UIExplorerPage');

var STORAGE_KEY = 'test';

var login = React.createClass({
		//TODO add checks and API submit
	getInitialState: function () {
        return {wNumber: "", password: ""};
  },
	async doLogin() {
  		//add helper for API to login
      try{
       let response = await fetch("http://wsudeploy-brysonwilding.rhcloud.com:80/api/users/login", {
         method: 'POST',
         headers: {"Authorization": AsyncStorage.setItem("access_token")},
         body: JSON.stringify({
           "username": "RickSanchez",
           "password": "megaseeds"
        })
      });
      let responseJson = await response.json();
      AsyncStorage.setItem("access_token", value.id);
      AsyncStorage.setItem("accessToken", value.id);
      AsyncStorage.setItem("id", value.userId);
      this.mainPage();
      }catch(error) {
        console.error(error);
      }
  },
	mainPage: function() {
    this.props.resetToRoute({
      name: "Main",
      component: main,
    });
  },
  render: function() {
    return (
      //<UIExplorerPage title="ToastAndroid">
      //<UIExplorerBlock title="Simple toast">
        <View style={styles.container}>
          <View style={styles.header}>
            <Text style={[styles.banner]}>
              WSU Athletics 
            </Text>
            <Image style={styles.picture} source={{uri: 'http://physics.weber.edu/carroll/PositionAd/WSUSEAL.GIF'}} />
          </View>
          <View style={styles.login}>
            <View style={styles.loginContainer}> 
                <TextInput
                placeholder="W Number"
                value={this.setState.wNumber}
                onChange={(wNumber) => this.setState({wNumber})}
                />
              </View>
              <View style={styles.loginContainer}>
                <TextInput
                placeholder="Password"
                value={this.setState.password}
                onChange={(password) => this.setState({password})}
                />
              </View>
          </View>
          <View style={styles.signin}>
            <TouchableHighlight onPress={this.doLogin}>
              <Text style={styles.whiteFont}>Sign In</Text>
            </TouchableHighlight>

          </View>

        </View>
        //</UIExplorerBlock>
        //</UIExplorerPage>
    );
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  banner: {
    position: 'relative',
    marginBottom: 10
  },
  bg: {
    position: 'absolute',
    left: 0,
    top: 0,
  },
  header: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: .25,
    backgroundColor: 'transparent'
  },
  picture: {
    width: 120,
    height: 120
  },
  login: {
    marginTop: 10,
    marginBottom: 10,
    flex: .25
  },
  loginContainer: {
    padding: 10,
    borderWidth: 1,
    borderBottomColor: '#CCC',
    borderColor: 'transparent'
  },
  signin: {
    backgroundColor: 'purple',
    padding: 10,
    alignItems: 'center',
    marginBottom: 50
  },
  whiteFont: {
    color: '#FFF'
  },
});


module.exports = login;