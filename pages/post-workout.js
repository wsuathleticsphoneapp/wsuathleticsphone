'use strict';

var React = require('react-native');

var {
	View,
	Text,
	AppRegistry,
	Component,
	TextInput,
	StyleSheet,
	TouchableHighlight,
	Picker,
} = React;

// //const DropDown = require('react-native-dropdown');

// const {
// 	Select,
// 	Option,
// 	OptionList,
// 	updatePosition
// } = DropDown;

var postworkout = React.createClass({
getInitialState: function() {
	return {
		time: 0,
		intensity: '',
		data:[],
	};
},

// componentDidMount: function() {
// 	updatePosition(this.refs['SELECT1']);
		
// 	updatePosition(this.refs['OPTIONLIST']);
// },
// 	getOptionList: function() {
// 		return this.refs['OPTIONLIST'];
// },
render: function() {
	return(
		<View style={styles.container}>
			<Text style={styles.whiteFont}>Workout time in minutes: </Text>
			<View style={styles.textEdit}>
				<TextInput
				height={34}
				width={199}
				onChangeText={(time) => this.setState({time})}
				value={this.state.text}
				/>
			</View>
			
			<View style={{ height: 10 }}/>

			<Text style={styles.whiteFont}>How intense was your workout: </Text>
			<View style={styles.textEdit}>
			<Picker 
  				selectedValue={this.state.intensity}
  				onValueChange={(intensity) => this.setState({intensity})}>
  				<Picker.Item label="0" value="0" />
  				<Picker.Item label="1" value="1" />
  				<Picker.Item label="2" value="2" />
  				<Picker.Item label="3" value="3" />
  				<Picker.Item label="4" value="4" />
  				<Picker.Item label="5" value="5" />
  				<Picker.Item label="6" value="6" />
  				<Picker.Item label="7" value="7" />
  				<Picker.Item label="8" value="8" />
  				<Picker.Item label="9" value="9" />
				<Picker.Item label="10" value="10" />
			</Picker>
			</View>

			<View style={{ height: 5 }}/>

			<View style={styles.button}>
          		<TouchableHighlight onPress={this.PLACEHOLDER}> 
            		<Text style={styles.whiteFont}>Submit</Text>
          		</TouchableHighlight>
        	</View>

		</View>
	)
}
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'grey',
    padding: 20,
  },
  textEdit: {
    
    borderColor: 'grey',
    backgroundColor: 'white',
    borderWidth: 1,
    width: 200
  },
  button: {
    backgroundColor: 'green',
    padding: 10,
    marginBottom: 50,
    width: 65
  },
  whiteFont: {
    color: '#FFF'
  },
});

module.exports = postworkout;