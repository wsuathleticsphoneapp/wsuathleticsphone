'use strict';
var React = require('react-native');

var amsurvey = require('./amsurvey');

var editaccount = require('./editaccount');

var postworkout = require('./post-workout');

var {
  AppRegistry,
  Component,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  Button
} = React;

var main = React.createClass ({
  amsurvey: function() {
    this.props.toRoute({
      name: "Morning Survey",
      component: amsurvey,
    });
  },

  editaccount: function() {
    this.props.toRoute({
      name: "Edit Account",
      component: editaccount,
    });
  },

  postworkout: function() {
    this.props.toRoute({
      name: "Post Workout Survey",
      component: postworkout,
    });
  },

  login: function() { //currently fails. unsure why something with navigator stack i think
    this.props.toRoute({
      name: "Login 2",
      component: login,
    });
  },

  render: function() {
    return (
      <View style={styles.container}>
        <View style={styles.list}>
          <View style={styles.buttonMakup}>
            <TouchableHighlight onPress={this.amsurvey}>
              <Text style={styles.whiteFont}>Morning Survey</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.buttonMakup}>
            <TouchableHighlight onPress={this.postworkout}>
              <Text style={styles.whiteFont}>Post Workout or Post Training Survey</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.buttonMakup}>
            <TouchableHighlight onPress={this.amsurvey}>
              <Text style={styles.whiteFont}>View Past Surveys</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.buttonMakup}>
            <TouchableHighlight onPress={this.editaccount}>
              <Text style={styles.whiteFont}>Edit Account</Text>
            </TouchableHighlight>
          </View>
          <View style={styles.buttonMakup}>
            <TouchableHighlight onPress={this.login}>
              <Text style={styles.whiteFont}>Sign Out</Text>
            </TouchableHighlight>
          </View>
        </View>
      </View>
    );
  }
});


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'grey',
    alignItems: 'center',
  },
  list: {
    paddingTop: 50,
  },
  
  buttonMakup: {
    backgroundColor: 'blue',
    padding: 10,
    alignItems: 'center',
    marginBottom: 20,
    width: 300,
  },
  whiteFont: {
    color: '#FFF'
  },
});


module.exports = main;
